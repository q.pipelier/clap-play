#ifndef FORMAT_H
#define FORMAT_H

#include <iostream>
#include <libpq-fe.h>

/**
 * \class Format 
 * \brief Allow to retrieve the attributes from the database
 */
class Format{
private:
  unsigned int id;
  std::string titled;
  
  
public:
 /**
  * Unique constructor which retrieve the value of the attributes from the database according to a track
  *
  * \param unsigned int - id_track : id of the track related to the format
  *
  * \param PGconn * - connexion : handler for the database connexion
  */
  Format(unsigned int, PGconn*);

  /**
   * \brief Get the id of the Format
   *
   * \return id : the id of the format as an unsigned integer
   */
  unsigned int getId();

  /**
   * \brief Get titled of the Format
   *
   * \return titled : titled of the format as a std::string
   */
  std::string getTitled();
};
#endif // FORMAT_H
