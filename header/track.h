#ifndef TRACK_H
#define TRACK_H

#include "artist.h"
#include "genre.h"
#include "album.h"
#include "format.h"
#include "subgenre.h"
#include "polyphony.h"
#include <iostream>
#include <Xspf.h>
#include <XspfTrack.h>
#include <libpq-fe.h>
#include <string>
#include <vector>

/**
 * \class Track
 *
 *\brief A class made to search and add new track
 *
 */
class Track{

private:
  int id;
  Xspf::XspfWriter *const playlist;
  PGconn *connexion;
  Album *album;
public:
  /**
   * \brief Constructor 
   *
   * \param int - id : id of the playlist
   */
  Track(int, Xspf::XspfWriter *const, PGconn *);

  /**
   * \brief Destructor
   *
   */
  ~Track();

  /**
   * \brief Search track by criterias, return vector with all id's of tracks
   *
   * \param uint_64 - duration_max : Duration max of the playlist
   * \param std::string - title : Title criteria
   * \param std::string - genre : Genre criteria
   * \param std::string - artist : Artist criteria
   * \param uint_64 - polyphony : Polyphony criteria
   * \param std::string - format : Format criteria
   *
   * \return vector<uint_64>
   *
   */
  std::vector<unsigned int> searchTrack(unsigned int, std::string, std::string, std::string, unsigned int, std::string);

  /**
   * \brief Add track according to id
   *
   * \param int - id : Id of the track
   *
   */
  void addTrack(int id);
  
};

#endif // TRACK_H
