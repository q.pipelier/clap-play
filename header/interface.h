#ifndef INTERFACE_H
#define INTERFACE_H

#include <gtkmm.h>
#include <iostream>

class Interface{

 protected:
 public:
  Interface(int argc, char* argv[]);

  class ModelColumns : public Gtk::TreeModel::ColumnRecord {
  public:
    ModelColumns(){
      add(m_col_id); add(m_col_name); add(m_col_format); add(m_col_taille);}

    Gtk::TreeModelColumn<unsigned int> m_col_id;
    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<Glib::ustring> m_col_format;
    Gtk::TreeModelColumn<int> m_col_taille;
  };

  ModelColumns m_Columns;

  void addElementPlaylist(Glib::RefPtr<Gtk::ListStore> m_refTreeModel);
  int getFileSize(const std::string &fileName);
};

#endif
