# Clap-play

**Objectif du projet:** `Création d'un générateur de playlist en C++ encadré par Grégory David`

## Documentation technique

http://www.bts-malraux72.net/~q.pipelier/ClapPlay/

## Documentation utilisateur

https://framagit.org/sio/ppe3/objectif100/valentin-quentin/-/tags/User-documentation

## Convention de codage

Ce projet suit une convention de codage particulière disponible librement aux url(s) suivantes: 

Vous pouvez les consulter sous les versions suivantes:

Markdown -> https://framagit.org/sio/ppe3/objectif100/valentin-quentin/wikis/Convention-de-codage

PDF -> Disponible bientôt