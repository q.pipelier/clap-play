#include <stdlib.h> 

#include "../header/subgenre.h"

Subgenre::Subgenre(unsigned int id_track, PGconn *connexion){
    std::string requete = "SELECT \"sousgenre\".id, \"sousgenre\".sous_type FROM \"sousgenre\" INNER JOIN \"morceau\" ON \"morceau\".fk_sousgenre = \"sousgenre\".id WHERE \"morceau\".id = $1;";

  const char *val[] = {std::to_string(id_track).c_str()};
  const Oid types[] = {20};

  PGresult *result_requete = PQexecParams(connexion,
                                         requete.c_str(),
                                         1,
                                         types,
                                         val,
                                         NULL,
                                         NULL,
                                         0);

   this->id = atoi(PQgetvalue(result_requete, 0, 0));
   this->subgenre = PQgetvalue(result_requete, 0, 1);
}

Subgenre::~Subgenre(){}

unsigned int Subgenre::getId(){
  return id;
}

std::string Subgenre::getSubgenre(){
  return subgenre;
}
