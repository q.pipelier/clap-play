#include "../header/file_configuration.h"
#include "../header/log.h"

FileConfiguration::FileConfiguration():
  config_db_host("host_default"),
  config_db_user("user default"),
  config_db_name("database default"),
  config_db_schema("schema default"),
  config_db_password("password default"),
  config_duration(0),
  config_loaded(false),
  config_title(""),
  config_m3u(true),
  config_xspf(false),
  config_genre(""),
  config_name_playlist("Name of the playlist")
{}

void FileConfiguration::load(std::string path)
{
  Json::Value config;
  std::ifstream file;
  file.open(path);
  std::string errors;
  Json::CharReaderBuilder builder_json;
  builder_json["collectComments"] = false;
  Log logger(true, "file_configuration.cc");
  
  if(file.is_open())
  {
    logger.log_info("Config file " + path + " has been found");
    config_loaded = true;

    if(Json::parseFromStream(builder_json, file, &config, &errors))
    {
      config_db_host = config["database"].get("hostname", config_db_host).asString();
      config_db_user = config["database"].get("user", config_db_user).asString();
      config_db_name = config["database"].get("name", config_db_name).asString();
      config_db_schema = config["database"].get("schema", config_db_schema).asString();
      config_db_password = config["database"].get("password", config_db_password).asString();
      config_duration = config["playlist"].get("duration", config_duration).asInt();
      config_title = config["playlist"].get("title", config_title).asString();
      config_m3u = config["playlist"].get("m3u", config_m3u).asBool();
      config_xspf = config["playlist"].get("xspf", config_xspf).asBool();
      config_genre = config["playlist"].get("genre", config_genre).asString();
      config_name_playlist = config["playlist"].get("name", config_name_playlist).asString();
      
    }
    else
    {
      logger.log_error("JSON is invalid");
    }

    file.close();
  }
  else
  {
    logger.log_warn("Config file " + path + " not found");
  }
}

bool FileConfiguration::getConfigLoaded()
{
  return config_loaded;
}

std::string FileConfiguration::getDbHost()
{
  return config_db_host;
}

std::string FileConfiguration::getDbName()
{
  return config_db_password;
}

std::string FileConfiguration::getDbUser()
{
  return config_db_user;
}

std::string FileConfiguration::getDbPassword()
{
  return config_db_password;
}

std::string FileConfiguration::getDbSchema()
{
  return config_db_schema;
}

unsigned int FileConfiguration::getDuration()
{
  return config_duration;
}

std::string FileConfiguration::getTitle()
{
  return config_title;
}

bool FileConfiguration::getM3u()
{
  return config_m3u;
}

bool FileConfiguration::getXspf(){
  return config_xspf;
}

std::string FileConfiguration::getGenre()
{
  return config_genre;
}

std::string FileConfiguration::getNamePlaylist()
{
  return config_name_playlist;
}
