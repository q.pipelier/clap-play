#include "../header/test_unit.h"
#include "../header/playlist.h"

void TestPlaylist::testDuration(){
    PGconn *retour_connexion = PQsetdbLogin("postgresql.bts-malraux72.net", "5432", nullptr, nullptr, "Cours", "q.pipelier", "Lilou1105");
    Playlist *playlist = new Playlist(1, "Playlits_test", 2569, "", retour_connexion);
    playlist->writeM3U("", "", "", "", "");

    CPPUNIT_ASSERT(playlist.getDuration() <= 2569);
}
