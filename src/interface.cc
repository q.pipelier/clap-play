#include "../header/interface.h"
#include <dirent.h>
#include <string>
#include <sys/types.h>
#include <fstream>

Interface::Interface(int argc, char* argv[])
{
  Gtk::Main app(argc, argv);
  Gtk::Window window;
  Gtk::Label label;
  Gtk::Notebook selector;

  Gtk::Grid grid;
  
  window.set_title("Clap Play");
  window.resize(400, 200);
  window.set_border_width(5);

  // Création des champs textes

  Gtk::Button button_generate;
  Gtk::Entry entry_name_playlist;
  entry_name_playlist.set_max_length(50);

  Gtk::Label label_name_playlist;
  label_name_playlist.set_text("Saisir le nom: ");
  grid.attach(label_name_playlist, 0, 0, 1, 1);

  Gtk::Label label_duration_playlist;
  label_duration_playlist.set_text("Saisir la durée: ");
  grid.attach(label_duration_playlist, 1, 0, 1, 1);
  Gtk::Entry entry_duration_playlist;

  grid.attach(entry_name_playlist, 0, 0, 2, 1);
  grid.attach(entry_duration_playlist, 1, 0, 2, 1);

  button_generate.add_label("Générer");

  grid.attach(button_generate, 0, 1, 3, 1);
  
  grid.show_all();
  
  label.set_text("page 1");

  // Liste des playlists

  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::TreeView m_TreeView;
  Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
  Gtk::Box m_VBox;

  m_ScrolledWindow.add(m_TreeView);
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  m_VBox.pack_start(m_ScrolledWindow);
  m_refTreeModel = Gtk::ListStore::create(m_Columns);

  m_TreeView.set_model(m_refTreeModel);

  // Ajout des fichiers playlists à la liste
  addElementPlaylist(m_refTreeModel);
  
  m_TreeView.append_column("Id", m_Columns.m_col_id);
  m_TreeView.append_column("Nom", m_Columns.m_col_name);
  m_TreeView.append_column("Format", m_Columns.m_col_format);
  m_TreeView.append_column("Taille", m_Columns.m_col_taille);
  
  selector.append_page(m_VBox, "Listes des playlists");
  selector.append_page(grid, "Créer une playlist");

  window.add(selector);
  window.show_all_children();
  
  Gtk::Main::run(window);
}

void Interface::addElementPlaylist(Glib::RefPtr<Gtk::ListStore> m_refTreeModel){

  const char*path = "./";

  std::string xspf = ".xspf";
  std::string m3u = ".m3u";
  
  struct dirent *entry;
  DIR *dir = opendir(path);

  int count = 0;
  
  if(dir == NULL){
    std::cerr << "Erreur, dossier null";
  }else{
    while((entry = readdir(dir)) != NULL){
      
      if(std::string(entry->d_name).find(xspf) != std::string::npos){
	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	count++;
	
	std::string name = std::string(entry->d_name).erase(std::string(entry->d_name).size()-5);

	row[m_Columns.m_col_id] = count;
	row[m_Columns.m_col_name] = name;
	row[m_Columns.m_col_format] = "XSPF";
	row[m_Columns.m_col_taille] = getFileSize(std::string(entry->d_name));
      }else if(std::string(entry->d_name).find(m3u) != std::string::npos){
	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	count++;

	std::string name = std::string(entry->d_name).erase(std::string(entry->d_name).size()-4);

	row[m_Columns.m_col_id] = count;
        row[m_Columns.m_col_name] = name;
        row[m_Columns.m_col_format] = "M3U";
        row[m_Columns.m_col_taille] = getFileSize(std::string(entry->d_name));
      }
    }    
    closedir(dir);
  }
}

int Interface::getFileSize(const std::string &fileName){

  std::ifstream file(fileName.c_str(), std::ifstream::in | std::ifstream::binary);

    if(!file.is_open())
    {
        return -1;
    }

    file.seekg(0, std::ios::end);
    int fileSize = file.tellg();
    file.close();

    return fileSize;
}
