#include <stdlib.h> 
#include <iostream>

#include "../header/polyphony.h"

Polyphony::Polyphony(unsigned int id_track, PGconn *connexion){
  std::string requete = "SELECT \"polyphonie\".id, \"polyphonie\".nombre FROM polyphonie INNER JOIN \"morceau\" ON \"morceau\".fk_polyphonie = \"polyphonie\".id WHERE \"morceau\".id = $1;";

  const char *val[] = {std::to_string(id_track).c_str()};
  const Oid types[] = {20};

  PGresult *result_requete = PQexecParams(connexion,
                                         requete.c_str(),
                                         1,
                                         types,
                                         val,
                                         NULL,
                                         NULL,
                                         0);

   this->id = atoi(PQgetvalue(result_requete, 0, 0));
   this->number = atoi(PQgetvalue(result_requete, 0, 1));
}

Polyphony::~Polyphony(){}

unsigned int Polyphony::getId(){
  return id;
}

unsigned int Polyphony::getNumber(){
  return number;
}
